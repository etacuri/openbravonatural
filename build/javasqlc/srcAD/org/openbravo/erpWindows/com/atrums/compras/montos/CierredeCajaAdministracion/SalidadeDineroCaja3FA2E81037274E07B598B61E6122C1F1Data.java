//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.compras.montos.CierredeCajaAdministracion;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class SalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data implements FieldProvider {
static Logger log4j = Logger.getLogger(SalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String hora;
  public String finPaymentmethodId;
  public String nroCheque;
  public String totalDeposito;
  public String salesId;
  public String salesIdr;
  public String entregado;
  public String fechaDeposito;
  public String finFinancialAccountId;
  public String nroDeposito;
  public String depositado;
  public String isactive;
  public String eliminardepo;
  public String ateccoCierrecajaId;
  public String adOrgId;
  public String adClientId;
  public String ateccoRetiroId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("hora"))
      return hora;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_id") || fieldName.equals("finPaymentmethodId"))
      return finPaymentmethodId;
    else if (fieldName.equalsIgnoreCase("nro_cheque") || fieldName.equals("nroCheque"))
      return nroCheque;
    else if (fieldName.equalsIgnoreCase("total_deposito") || fieldName.equals("totalDeposito"))
      return totalDeposito;
    else if (fieldName.equalsIgnoreCase("sales_id") || fieldName.equals("salesId"))
      return salesId;
    else if (fieldName.equalsIgnoreCase("sales_idr") || fieldName.equals("salesIdr"))
      return salesIdr;
    else if (fieldName.equalsIgnoreCase("entregado"))
      return entregado;
    else if (fieldName.equalsIgnoreCase("fecha_deposito") || fieldName.equals("fechaDeposito"))
      return fechaDeposito;
    else if (fieldName.equalsIgnoreCase("fin_financial_account_id") || fieldName.equals("finFinancialAccountId"))
      return finFinancialAccountId;
    else if (fieldName.equalsIgnoreCase("nro_deposito") || fieldName.equals("nroDeposito"))
      return nroDeposito;
    else if (fieldName.equalsIgnoreCase("depositado"))
      return depositado;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("eliminardepo"))
      return eliminardepo;
    else if (fieldName.equalsIgnoreCase("atecco_cierrecaja_id") || fieldName.equals("ateccoCierrecajaId"))
      return ateccoCierrecajaId;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("atecco_retiro_id") || fieldName.equals("ateccoRetiroId"))
      return ateccoRetiroId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static SalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String ateccoCierrecajaId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, ateccoCierrecajaId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static SalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String ateccoCierrecajaId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(atecco_retiro.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = atecco_retiro.CreatedBy) as CreatedByR, " +
      "        to_char(atecco_retiro.Updated, ?) as updated, " +
      "        to_char(atecco_retiro.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        atecco_retiro.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = atecco_retiro.UpdatedBy) as UpdatedByR," +
      "        TO_CHAR(atecco_retiro.Hora, ?) AS Hora, " +
      "atecco_retiro.FIN_Paymentmethod_ID, " +
      "atecco_retiro.Nro_Cheque, " +
      "atecco_retiro.Total_Deposito, " +
      "atecco_retiro.Sales_ID, " +
      "(CASE WHEN atecco_retiro.Sales_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS Sales_IDR, " +
      "COALESCE(atecco_retiro.Entregado, 'N') AS Entregado, " +
      "TO_CHAR(atecco_retiro.Fecha_Deposito, ?) AS Fecha_Deposito, " +
      "atecco_retiro.FIN_Financial_Account_ID, " +
      "atecco_retiro.Nro_Deposito, " +
      "COALESCE(atecco_retiro.Depositado, 'N') AS Depositado, " +
      "COALESCE(atecco_retiro.Isactive, 'N') AS Isactive, " +
      "atecco_retiro.Eliminardepo, " +
      "atecco_retiro.Atecco_Cierrecaja_ID, " +
      "atecco_retiro.AD_Org_ID, " +
      "atecco_retiro.AD_Client_ID, " +
      "atecco_retiro.Atecco_Retiro_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM atecco_retiro left join (select AD_User_ID, Name from AD_User) table1 on (atecco_retiro.Sales_ID =  table1.AD_User_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((ateccoCierrecajaId==null || ateccoCierrecajaId.equals(""))?"":"  AND atecco_retiro.Atecco_Cierrecaja_ID = ?  ");
    strSql = strSql + 
      "        AND atecco_retiro.Atecco_Retiro_ID = ? " +
      "        AND atecco_retiro.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND atecco_retiro.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (ateccoCierrecajaId != null && !(ateccoCierrecajaId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoCierrecajaId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        SalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data = new SalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data();
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.created = UtilSql.getValue(result, "created");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.updated = UtilSql.getValue(result, "updated");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.hora = UtilSql.getValue(result, "hora");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.finPaymentmethodId = UtilSql.getValue(result, "fin_paymentmethod_id");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.nroCheque = UtilSql.getValue(result, "nro_cheque");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.totalDeposito = UtilSql.getValue(result, "total_deposito");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.salesId = UtilSql.getValue(result, "sales_id");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.salesIdr = UtilSql.getValue(result, "sales_idr");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.entregado = UtilSql.getValue(result, "entregado");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.fechaDeposito = UtilSql.getValue(result, "fecha_deposito");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.finFinancialAccountId = UtilSql.getValue(result, "fin_financial_account_id");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.nroDeposito = UtilSql.getValue(result, "nro_deposito");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.depositado = UtilSql.getValue(result, "depositado");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.isactive = UtilSql.getValue(result, "isactive");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.eliminardepo = UtilSql.getValue(result, "eliminardepo");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.ateccoCierrecajaId = UtilSql.getValue(result, "atecco_cierrecaja_id");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.ateccoRetiroId = UtilSql.getValue(result, "atecco_retiro_id");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.language = UtilSql.getValue(result, "language");
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.adUserClient = "";
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.adOrgClient = "";
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.createdby = "";
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.trBgcolor = "";
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.totalCount = "";
        objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    SalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[] = new SalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[vector.size()];
    vector.copyInto(objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data);
    return(objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data);
  }

/**
Create a registry
 */
  public static SalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[] set(String ateccoCierrecajaId, String finFinancialAccountId, String updatedby, String updatedbyr, String nroCheque, String ateccoRetiroId, String salesId, String hora, String adClientId, String entregado, String finPaymentmethodId, String totalDeposito, String adOrgId, String eliminardepo, String isactive, String nroDeposito, String fechaDeposito, String createdby, String createdbyr, String depositado)    throws ServletException {
    SalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[] = new SalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[1];
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0] = new SalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data();
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].created = "";
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].createdbyr = createdbyr;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].updated = "";
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].updatedTimeStamp = "";
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].updatedby = updatedby;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].updatedbyr = updatedbyr;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].hora = hora;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].finPaymentmethodId = finPaymentmethodId;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].nroCheque = nroCheque;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].totalDeposito = totalDeposito;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].salesId = salesId;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].salesIdr = "";
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].entregado = entregado;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].fechaDeposito = fechaDeposito;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].finFinancialAccountId = finFinancialAccountId;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].nroDeposito = nroDeposito;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].depositado = depositado;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].isactive = isactive;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].eliminardepo = eliminardepo;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].ateccoCierrecajaId = ateccoCierrecajaId;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].adOrgId = adOrgId;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].adClientId = adClientId;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].ateccoRetiroId = ateccoRetiroId;
    objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data[0].language = "";
    return objectSalidadeDineroCaja3FA2E81037274E07B598B61E6122C1F1Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef1B9BE23DC9A4474F99FEBF7517A3F74F_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefC987FEEE88D04A4C84BF461E19A2F239_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT atecco_retiro.Atecco_Cierrecaja_ID AS NAME" +
      "        FROM atecco_retiro" +
      "        WHERE atecco_retiro.Atecco_Retiro_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String ateccoCierrecajaId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(table1.Fecha_Apertura, 'DD-MM-YYYY HH24:MM:SS')) AS NAME FROM atecco_cierrecaja left join (select Atecco_Cierrecaja_ID, Fecha_Apertura from Atecco_Cierrecaja) table1 on (atecco_cierrecaja.Atecco_Cierrecaja_ID = table1.Atecco_Cierrecaja_ID) WHERE atecco_cierrecaja.Atecco_Cierrecaja_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoCierrecajaId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String ateccoCierrecajaId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(table1.Fecha_Apertura, 'DD-MM-YYYY HH24:MM:SS')) AS NAME FROM atecco_cierrecaja left join (select Atecco_Cierrecaja_ID, Fecha_Apertura from Atecco_Cierrecaja) table1 on (atecco_cierrecaja.Atecco_Cierrecaja_ID = table1.Atecco_Cierrecaja_ID) WHERE atecco_cierrecaja.Atecco_Cierrecaja_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoCierrecajaId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE atecco_retiro" +
      "        SET Hora = TO_TIMESTAMP(?, ?) , FIN_Paymentmethod_ID = (?) , Nro_Cheque = (?) , Total_Deposito = TO_NUMBER(?) , Sales_ID = (?) , Entregado = (?) , Fecha_Deposito = TO_TIMESTAMP(?, ?) , FIN_Financial_Account_ID = (?) , Nro_Deposito = (?) , Depositado = (?) , Isactive = (?) , Eliminardepo = (?) , Atecco_Cierrecaja_ID = (?) , AD_Org_ID = (?) , AD_Client_ID = (?) , Atecco_Retiro_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE atecco_retiro.Atecco_Retiro_ID = ? " +
      "                 AND atecco_retiro.Atecco_Cierrecaja_ID = ? " +
      "        AND atecco_retiro.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND atecco_retiro.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, hora);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nroCheque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalDeposito);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, entregado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaDeposito);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nroDeposito);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, depositado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, eliminardepo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoCierrecajaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoRetiroId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoRetiroId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoCierrecajaId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO atecco_retiro " +
      "        (Hora, FIN_Paymentmethod_ID, Nro_Cheque, Total_Deposito, Sales_ID, Entregado, Fecha_Deposito, FIN_Financial_Account_ID, Nro_Deposito, Depositado, Isactive, Eliminardepo, Atecco_Cierrecaja_ID, AD_Org_ID, AD_Client_ID, Atecco_Retiro_ID, created, createdby, updated, updatedBy)" +
      "        VALUES (TO_TIMESTAMP(?, ?), (?), (?), TO_NUMBER(?), (?), (?), TO_TIMESTAMP(?, ?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, hora);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nroCheque);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalDeposito);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, entregado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaDeposito);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nroDeposito);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, depositado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, eliminardepo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoCierrecajaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoRetiroId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String ateccoCierrecajaId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM atecco_retiro" +
      "        WHERE atecco_retiro.Atecco_Retiro_ID = ? " +
      "                 AND atecco_retiro.Atecco_Cierrecaja_ID = ? " +
      "        AND atecco_retiro.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND atecco_retiro.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoCierrecajaId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM atecco_retiro" +
      "         WHERE atecco_retiro.Atecco_Retiro_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM atecco_retiro" +
      "         WHERE atecco_retiro.Atecco_Retiro_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
