//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.compras.montos.ConsultadeProductosVenta;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Producto616FA0E755174D728C5E3C5222AEF9ABData implements FieldProvider {
static Logger log4j = Logger.getLogger(Producto616FA0E755174D728C5E3C5222AEF9ABData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String emAteccoFamiliaId;
  public String emAteccoGenericoId;
  public String adImageId;
  public String mBrandId;
  public String mBrandIdr;
  public String producttype;
  public String producttyper;
  public String name2;
  public String isgeneric;
  public String emAteccoModelo;
  public String value;
  public String emAteccoCodigoEdimca;
  public String emAteccoCodigoCotopaxi;
  public String genericProductId;
  public String emAteccoTipo;
  public String emAteccoCaras;
  public String emAteccoColor1;
  public String emAteccoColor2;
  public String emAteccoD1;
  public String weight;
  public String upc;
  public String cUomWeightId;
  public String mProductCategoryId;
  public String mProductCategoryIdr;
  public String costtype;
  public String costtyper;
  public String mAttributesetId;
  public String cTaxcategoryId;
  public String cTaxcategoryIdr;
  public String emAteccoD2;
  public String emAteccoEspesor;
  public String emAteccoM3;
  public String name;
  public String description;
  public String cUomId;
  public String cUomIdr;
  public String emAteccoTextura;
  public String delaymin;
  public String isstocked;
  public String salesrepId;
  public String issold;
  public String ispurchased;
  public String isbom;
  public String isactive;
  public String cBpartnerId;
  public String imageurl;
  public String descriptionurl;
  public String issummary;
  public String mLocatorId;
  public String volume;
  public String shelfwidth;
  public String shelfheight;
  public String shelfdepth;
  public String unitsperpallet;
  public String discontinued;
  public String discontinuedby;
  public String isinvoiceprintdetails;
  public String ispicklistprintdetails;
  public String isverified;
  public String sExpensetypeId;
  public String isquantityvariable;
  public String sResourceId;
  public String expplantype;
  public String periodnumberExp;
  public String defaultperiodExp;
  public String calculated;
  public String capacity;
  public String mrpPlannerId;
  public String mrpPlanningmethodId;
  public String qtymax;
  public String qtymin;
  public String qtystd;
  public String qtytype;
  public String stockmin;
  public String createvariants;
  public String updateinvariants;
  public String islinkedtoproduct;
  public String prodCatSelection;
  public String productSelection;
  public String returnable;
  public String overdueReturnDays;
  public String ispricerulebased;
  public String quantityRule;
  public String uniquePerDocument;
  public String printDescription;
  public String relateprodcattoservice;
  public String relateprodtoservice;
  public String allowDeferredSell;
  public String deferredSellMaxDays;
  public String attrsetvaluetype;
  public String mProductId;
  public String classification;
  public String adClientId;
  public String sku;
  public String documentnote;
  public String help;
  public String processing;
  public String stockMin;
  public String enforceAttribute;
  public String maProcessplanId;
  public String guaranteedays;
  public String mAttributesetinstanceId;
  public String downloadurl;
  public String mFreightcategoryId;
  public String ispriceprinted;
  public String production;
  public String coststd;
  public String characteristicDesc;
  public String isdeferredrevenue;
  public String bookusingpoprice;
  public String periodnumber;
  public String defaultperiod;
  public String revplantype;
  public String isdeferredexpense;
  public String managevariants;
  public String versionno;
  public String emAteccoCategoriaId;
  public String emAteccoSubCategoriaId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("em_atecco_familia_id") || fieldName.equals("emAteccoFamiliaId"))
      return emAteccoFamiliaId;
    else if (fieldName.equalsIgnoreCase("em_atecco_generico_id") || fieldName.equals("emAteccoGenericoId"))
      return emAteccoGenericoId;
    else if (fieldName.equalsIgnoreCase("ad_image_id") || fieldName.equals("adImageId"))
      return adImageId;
    else if (fieldName.equalsIgnoreCase("m_brand_id") || fieldName.equals("mBrandId"))
      return mBrandId;
    else if (fieldName.equalsIgnoreCase("m_brand_idr") || fieldName.equals("mBrandIdr"))
      return mBrandIdr;
    else if (fieldName.equalsIgnoreCase("producttype"))
      return producttype;
    else if (fieldName.equalsIgnoreCase("producttyper"))
      return producttyper;
    else if (fieldName.equalsIgnoreCase("name2"))
      return name2;
    else if (fieldName.equalsIgnoreCase("isgeneric"))
      return isgeneric;
    else if (fieldName.equalsIgnoreCase("em_atecco_modelo") || fieldName.equals("emAteccoModelo"))
      return emAteccoModelo;
    else if (fieldName.equalsIgnoreCase("value"))
      return value;
    else if (fieldName.equalsIgnoreCase("em_atecco_codigo_edimca") || fieldName.equals("emAteccoCodigoEdimca"))
      return emAteccoCodigoEdimca;
    else if (fieldName.equalsIgnoreCase("em_atecco_codigo_cotopaxi") || fieldName.equals("emAteccoCodigoCotopaxi"))
      return emAteccoCodigoCotopaxi;
    else if (fieldName.equalsIgnoreCase("generic_product_id") || fieldName.equals("genericProductId"))
      return genericProductId;
    else if (fieldName.equalsIgnoreCase("em_atecco_tipo") || fieldName.equals("emAteccoTipo"))
      return emAteccoTipo;
    else if (fieldName.equalsIgnoreCase("em_atecco_caras") || fieldName.equals("emAteccoCaras"))
      return emAteccoCaras;
    else if (fieldName.equalsIgnoreCase("em_atecco_color1") || fieldName.equals("emAteccoColor1"))
      return emAteccoColor1;
    else if (fieldName.equalsIgnoreCase("em_atecco_color2") || fieldName.equals("emAteccoColor2"))
      return emAteccoColor2;
    else if (fieldName.equalsIgnoreCase("em_atecco_d1") || fieldName.equals("emAteccoD1"))
      return emAteccoD1;
    else if (fieldName.equalsIgnoreCase("weight"))
      return weight;
    else if (fieldName.equalsIgnoreCase("upc"))
      return upc;
    else if (fieldName.equalsIgnoreCase("c_uom_weight_id") || fieldName.equals("cUomWeightId"))
      return cUomWeightId;
    else if (fieldName.equalsIgnoreCase("m_product_category_id") || fieldName.equals("mProductCategoryId"))
      return mProductCategoryId;
    else if (fieldName.equalsIgnoreCase("m_product_category_idr") || fieldName.equals("mProductCategoryIdr"))
      return mProductCategoryIdr;
    else if (fieldName.equalsIgnoreCase("costtype"))
      return costtype;
    else if (fieldName.equalsIgnoreCase("costtyper"))
      return costtyper;
    else if (fieldName.equalsIgnoreCase("m_attributeset_id") || fieldName.equals("mAttributesetId"))
      return mAttributesetId;
    else if (fieldName.equalsIgnoreCase("c_taxcategory_id") || fieldName.equals("cTaxcategoryId"))
      return cTaxcategoryId;
    else if (fieldName.equalsIgnoreCase("c_taxcategory_idr") || fieldName.equals("cTaxcategoryIdr"))
      return cTaxcategoryIdr;
    else if (fieldName.equalsIgnoreCase("em_atecco_d2") || fieldName.equals("emAteccoD2"))
      return emAteccoD2;
    else if (fieldName.equalsIgnoreCase("em_atecco_espesor") || fieldName.equals("emAteccoEspesor"))
      return emAteccoEspesor;
    else if (fieldName.equalsIgnoreCase("em_atecco_m3") || fieldName.equals("emAteccoM3"))
      return emAteccoM3;
    else if (fieldName.equalsIgnoreCase("name"))
      return name;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("c_uom_id") || fieldName.equals("cUomId"))
      return cUomId;
    else if (fieldName.equalsIgnoreCase("c_uom_idr") || fieldName.equals("cUomIdr"))
      return cUomIdr;
    else if (fieldName.equalsIgnoreCase("em_atecco_textura") || fieldName.equals("emAteccoTextura"))
      return emAteccoTextura;
    else if (fieldName.equalsIgnoreCase("delaymin"))
      return delaymin;
    else if (fieldName.equalsIgnoreCase("isstocked"))
      return isstocked;
    else if (fieldName.equalsIgnoreCase("salesrep_id") || fieldName.equals("salesrepId"))
      return salesrepId;
    else if (fieldName.equalsIgnoreCase("issold"))
      return issold;
    else if (fieldName.equalsIgnoreCase("ispurchased"))
      return ispurchased;
    else if (fieldName.equalsIgnoreCase("isbom"))
      return isbom;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("imageurl"))
      return imageurl;
    else if (fieldName.equalsIgnoreCase("descriptionurl"))
      return descriptionurl;
    else if (fieldName.equalsIgnoreCase("issummary"))
      return issummary;
    else if (fieldName.equalsIgnoreCase("m_locator_id") || fieldName.equals("mLocatorId"))
      return mLocatorId;
    else if (fieldName.equalsIgnoreCase("volume"))
      return volume;
    else if (fieldName.equalsIgnoreCase("shelfwidth"))
      return shelfwidth;
    else if (fieldName.equalsIgnoreCase("shelfheight"))
      return shelfheight;
    else if (fieldName.equalsIgnoreCase("shelfdepth"))
      return shelfdepth;
    else if (fieldName.equalsIgnoreCase("unitsperpallet"))
      return unitsperpallet;
    else if (fieldName.equalsIgnoreCase("discontinued"))
      return discontinued;
    else if (fieldName.equalsIgnoreCase("discontinuedby"))
      return discontinuedby;
    else if (fieldName.equalsIgnoreCase("isinvoiceprintdetails"))
      return isinvoiceprintdetails;
    else if (fieldName.equalsIgnoreCase("ispicklistprintdetails"))
      return ispicklistprintdetails;
    else if (fieldName.equalsIgnoreCase("isverified"))
      return isverified;
    else if (fieldName.equalsIgnoreCase("s_expensetype_id") || fieldName.equals("sExpensetypeId"))
      return sExpensetypeId;
    else if (fieldName.equalsIgnoreCase("isquantityvariable"))
      return isquantityvariable;
    else if (fieldName.equalsIgnoreCase("s_resource_id") || fieldName.equals("sResourceId"))
      return sResourceId;
    else if (fieldName.equalsIgnoreCase("expplantype"))
      return expplantype;
    else if (fieldName.equalsIgnoreCase("periodnumber_exp") || fieldName.equals("periodnumberExp"))
      return periodnumberExp;
    else if (fieldName.equalsIgnoreCase("defaultperiod_exp") || fieldName.equals("defaultperiodExp"))
      return defaultperiodExp;
    else if (fieldName.equalsIgnoreCase("calculated"))
      return calculated;
    else if (fieldName.equalsIgnoreCase("capacity"))
      return capacity;
    else if (fieldName.equalsIgnoreCase("mrp_planner_id") || fieldName.equals("mrpPlannerId"))
      return mrpPlannerId;
    else if (fieldName.equalsIgnoreCase("mrp_planningmethod_id") || fieldName.equals("mrpPlanningmethodId"))
      return mrpPlanningmethodId;
    else if (fieldName.equalsIgnoreCase("qtymax"))
      return qtymax;
    else if (fieldName.equalsIgnoreCase("qtymin"))
      return qtymin;
    else if (fieldName.equalsIgnoreCase("qtystd"))
      return qtystd;
    else if (fieldName.equalsIgnoreCase("qtytype"))
      return qtytype;
    else if (fieldName.equalsIgnoreCase("stockmin"))
      return stockmin;
    else if (fieldName.equalsIgnoreCase("createvariants"))
      return createvariants;
    else if (fieldName.equalsIgnoreCase("updateinvariants"))
      return updateinvariants;
    else if (fieldName.equalsIgnoreCase("islinkedtoproduct"))
      return islinkedtoproduct;
    else if (fieldName.equalsIgnoreCase("prod_cat_selection") || fieldName.equals("prodCatSelection"))
      return prodCatSelection;
    else if (fieldName.equalsIgnoreCase("product_selection") || fieldName.equals("productSelection"))
      return productSelection;
    else if (fieldName.equalsIgnoreCase("returnable"))
      return returnable;
    else if (fieldName.equalsIgnoreCase("overdue_return_days") || fieldName.equals("overdueReturnDays"))
      return overdueReturnDays;
    else if (fieldName.equalsIgnoreCase("ispricerulebased"))
      return ispricerulebased;
    else if (fieldName.equalsIgnoreCase("quantity_rule") || fieldName.equals("quantityRule"))
      return quantityRule;
    else if (fieldName.equalsIgnoreCase("unique_per_document") || fieldName.equals("uniquePerDocument"))
      return uniquePerDocument;
    else if (fieldName.equalsIgnoreCase("print_description") || fieldName.equals("printDescription"))
      return printDescription;
    else if (fieldName.equalsIgnoreCase("relateprodcattoservice"))
      return relateprodcattoservice;
    else if (fieldName.equalsIgnoreCase("relateprodtoservice"))
      return relateprodtoservice;
    else if (fieldName.equalsIgnoreCase("allow_deferred_sell") || fieldName.equals("allowDeferredSell"))
      return allowDeferredSell;
    else if (fieldName.equalsIgnoreCase("deferred_sell_max_days") || fieldName.equals("deferredSellMaxDays"))
      return deferredSellMaxDays;
    else if (fieldName.equalsIgnoreCase("attrsetvaluetype"))
      return attrsetvaluetype;
    else if (fieldName.equalsIgnoreCase("m_product_id") || fieldName.equals("mProductId"))
      return mProductId;
    else if (fieldName.equalsIgnoreCase("classification"))
      return classification;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("sku"))
      return sku;
    else if (fieldName.equalsIgnoreCase("documentnote"))
      return documentnote;
    else if (fieldName.equalsIgnoreCase("help"))
      return help;
    else if (fieldName.equalsIgnoreCase("processing"))
      return processing;
    else if (fieldName.equalsIgnoreCase("stock_min") || fieldName.equals("stockMin"))
      return stockMin;
    else if (fieldName.equalsIgnoreCase("enforce_attribute") || fieldName.equals("enforceAttribute"))
      return enforceAttribute;
    else if (fieldName.equalsIgnoreCase("ma_processplan_id") || fieldName.equals("maProcessplanId"))
      return maProcessplanId;
    else if (fieldName.equalsIgnoreCase("guaranteedays"))
      return guaranteedays;
    else if (fieldName.equalsIgnoreCase("m_attributesetinstance_id") || fieldName.equals("mAttributesetinstanceId"))
      return mAttributesetinstanceId;
    else if (fieldName.equalsIgnoreCase("downloadurl"))
      return downloadurl;
    else if (fieldName.equalsIgnoreCase("m_freightcategory_id") || fieldName.equals("mFreightcategoryId"))
      return mFreightcategoryId;
    else if (fieldName.equalsIgnoreCase("ispriceprinted"))
      return ispriceprinted;
    else if (fieldName.equalsIgnoreCase("production"))
      return production;
    else if (fieldName.equalsIgnoreCase("coststd"))
      return coststd;
    else if (fieldName.equalsIgnoreCase("characteristic_desc") || fieldName.equals("characteristicDesc"))
      return characteristicDesc;
    else if (fieldName.equalsIgnoreCase("isdeferredrevenue"))
      return isdeferredrevenue;
    else if (fieldName.equalsIgnoreCase("bookusingpoprice"))
      return bookusingpoprice;
    else if (fieldName.equalsIgnoreCase("periodnumber"))
      return periodnumber;
    else if (fieldName.equalsIgnoreCase("defaultperiod"))
      return defaultperiod;
    else if (fieldName.equalsIgnoreCase("revplantype"))
      return revplantype;
    else if (fieldName.equalsIgnoreCase("isdeferredexpense"))
      return isdeferredexpense;
    else if (fieldName.equalsIgnoreCase("managevariants"))
      return managevariants;
    else if (fieldName.equalsIgnoreCase("versionno"))
      return versionno;
    else if (fieldName.equalsIgnoreCase("em_atecco_categoria_id") || fieldName.equals("emAteccoCategoriaId"))
      return emAteccoCategoriaId;
    else if (fieldName.equalsIgnoreCase("em_atecco_sub_categoria_id") || fieldName.equals("emAteccoSubCategoriaId"))
      return emAteccoSubCategoriaId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Producto616FA0E755174D728C5E3C5222AEF9ABData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Producto616FA0E755174D728C5E3C5222AEF9ABData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(M_Product.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = M_Product.CreatedBy) as CreatedByR, " +
      "        to_char(M_Product.Updated, ?) as updated, " +
      "        to_char(M_Product.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        M_Product.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = M_Product.UpdatedBy) as UpdatedByR," +
      "        M_Product.AD_Org_ID, " +
      "(CASE WHEN M_Product.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "M_Product.EM_Atecco_Familia_ID, " +
      "M_Product.EM_Atecco_Generico_ID, " +
      "M_Product.AD_Image_ID, " +
      "M_Product.M_Brand_ID, " +
      "(CASE WHEN M_Product.M_Brand_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS M_Brand_IDR, " +
      "M_Product.ProductType, " +
      "(CASE WHEN M_Product.ProductType IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS ProductTypeR, " +
      "M_Product.Name2, " +
      "COALESCE(M_Product.IsGeneric, 'N') AS IsGeneric, " +
      "M_Product.EM_Atecco_Modelo, " +
      "M_Product.Value, " +
      "M_Product.EM_Atecco_Codigo_Edimca, " +
      "M_Product.EM_Atecco_Codigo_Cotopaxi, " +
      "M_Product.Generic_Product_ID, " +
      "M_Product.EM_Atecco_Tipo, " +
      "M_Product.EM_Atecco_Caras, " +
      "M_Product.EM_Atecco_Color1, " +
      "M_Product.EM_Atecco_Color2, " +
      "M_Product.EM_Atecco_D1, " +
      "M_Product.Weight, " +
      "M_Product.UPC, " +
      "M_Product.C_Uom_Weight_ID, " +
      "M_Product.M_Product_Category_ID, " +
      "(CASE WHEN M_Product.M_Product_Category_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL3.Name IS NULL THEN TO_CHAR(table3.Name) ELSE TO_CHAR(tableTRL3.Name) END)), ''))),'') ) END) AS M_Product_Category_IDR, " +
      "M_Product.Costtype, " +
      "(CASE WHEN M_Product.Costtype IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS CosttypeR, " +
      "M_Product.M_AttributeSet_ID, " +
      "M_Product.C_TaxCategory_ID, " +
      "(CASE WHEN M_Product.C_TaxCategory_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL5.Name IS NULL THEN TO_CHAR(table5.Name) ELSE TO_CHAR(tableTRL5.Name) END)), ''))),'') ) END) AS C_TaxCategory_IDR, " +
      "M_Product.EM_Atecco_D2, " +
      "M_Product.EM_Atecco_Espesor, " +
      "M_Product.EM_Atecco_M3, " +
      "M_Product.Name, " +
      "M_Product.Description, " +
      "M_Product.C_UOM_ID, " +
      "(CASE WHEN M_Product.C_UOM_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL7.Name IS NULL THEN TO_CHAR(table7.Name) ELSE TO_CHAR(tableTRL7.Name) END)), ''))),'') ) END) AS C_UOM_IDR, " +
      "M_Product.EM_Atecco_Textura, " +
      "M_Product.Delaymin, " +
      "COALESCE(M_Product.IsStocked, 'N') AS IsStocked, " +
      "M_Product.SalesRep_ID, " +
      "COALESCE(M_Product.IsSold, 'N') AS IsSold, " +
      "COALESCE(M_Product.IsPurchased, 'N') AS IsPurchased, " +
      "COALESCE(M_Product.IsBOM, 'N') AS IsBOM, " +
      "COALESCE(M_Product.IsActive, 'N') AS IsActive, " +
      "M_Product.C_BPartner_ID, " +
      "M_Product.ImageURL, " +
      "M_Product.DescriptionURL, " +
      "COALESCE(M_Product.IsSummary, 'N') AS IsSummary, " +
      "M_Product.M_Locator_ID, " +
      "M_Product.Volume, " +
      "M_Product.ShelfWidth, " +
      "M_Product.ShelfHeight, " +
      "M_Product.ShelfDepth, " +
      "M_Product.UnitsPerPallet, " +
      "COALESCE(M_Product.Discontinued, 'N') AS Discontinued, " +
      "M_Product.DiscontinuedBy, " +
      "COALESCE(M_Product.IsInvoicePrintDetails, 'N') AS IsInvoicePrintDetails, " +
      "COALESCE(M_Product.IsPickListPrintDetails, 'N') AS IsPickListPrintDetails, " +
      "COALESCE(M_Product.IsVerified, 'N') AS IsVerified, " +
      "M_Product.S_ExpenseType_ID, " +
      "COALESCE(M_Product.Isquantityvariable, 'N') AS Isquantityvariable, " +
      "M_Product.S_Resource_ID, " +
      "M_Product.Expplantype, " +
      "M_Product.Periodnumber_Exp, " +
      "M_Product.DefaultPeriod_Exp, " +
      "COALESCE(M_Product.Calculated, 'N') AS Calculated, " +
      "M_Product.Capacity, " +
      "M_Product.MRP_Planner_ID, " +
      "M_Product.MRP_Planningmethod_ID, " +
      "M_Product.Qtymax, " +
      "M_Product.Qtymin, " +
      "M_Product.Qtystd, " +
      "COALESCE(M_Product.Qtytype, 'N') AS Qtytype, " +
      "M_Product.Stockmin, " +
      "M_Product.CreateVariants, " +
      "M_Product.Updateinvariants, " +
      "COALESCE(M_Product.Islinkedtoproduct, 'N') AS Islinkedtoproduct, " +
      "M_Product.Prod_Cat_Selection, " +
      "M_Product.Product_Selection, " +
      "COALESCE(M_Product.Returnable, 'N') AS Returnable, " +
      "M_Product.Overdue_Return_Days, " +
      "COALESCE(M_Product.Ispricerulebased, 'N') AS Ispricerulebased, " +
      "M_Product.Quantity_Rule, " +
      "COALESCE(M_Product.Unique_Per_Document, 'N') AS Unique_Per_Document, " +
      "COALESCE(M_Product.Print_Description, 'N') AS Print_Description, " +
      "M_Product.Relateprodcattoservice, " +
      "M_Product.Relateprodtoservice, " +
      "COALESCE(M_Product.Allow_Deferred_Sell, 'N') AS Allow_Deferred_Sell, " +
      "M_Product.Deferred_Sell_Max_Days, " +
      "M_Product.Attrsetvaluetype, " +
      "M_Product.M_Product_ID, " +
      "M_Product.Classification, " +
      "M_Product.AD_Client_ID, " +
      "M_Product.SKU, " +
      "M_Product.DocumentNote, " +
      "M_Product.Help, " +
      "M_Product.Processing, " +
      "M_Product.Stock_Min, " +
      "COALESCE(M_Product.Enforce_Attribute, 'N') AS Enforce_Attribute, " +
      "M_Product.MA_Processplan_ID, " +
      "M_Product.GuaranteeDays, " +
      "M_Product.M_AttributeSetInstance_ID, " +
      "M_Product.DownloadURL, " +
      "M_Product.M_FreightCategory_ID, " +
      "COALESCE(M_Product.Ispriceprinted, 'N') AS Ispriceprinted, " +
      "COALESCE(M_Product.Production, 'N') AS Production, " +
      "M_Product.Coststd, " +
      "M_Product.Characteristic_Desc, " +
      "COALESCE(M_Product.Isdeferredrevenue, 'N') AS Isdeferredrevenue, " +
      "COALESCE(M_Product.Bookusingpoprice, 'N') AS Bookusingpoprice, " +
      "M_Product.Periodnumber, " +
      "M_Product.DefaultPeriod, " +
      "M_Product.Revplantype, " +
      "COALESCE(M_Product.Isdeferredexpense, 'N') AS Isdeferredexpense, " +
      "M_Product.ManageVariants, " +
      "M_Product.VersionNo, " +
      "M_Product.EM_Atecco_Categoria_ID, " +
      "M_Product.EM_Atecco_Sub_Categoria_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM M_Product left join (select AD_Org_ID, Name from AD_Org) table1 on (M_Product.AD_Org_ID = table1.AD_Org_ID) left join (select M_Brand_ID, Name from M_Brand) table2 on (M_Product.M_Brand_ID = table2.M_Brand_ID) left join ad_ref_list_v list1 on (M_Product.ProductType = list1.value and list1.ad_reference_id = '270' and list1.ad_language = ?)  left join (select M_Product_Category_ID, Name from M_Product_Category) table3 on (M_Product.M_Product_Category_ID = table3.M_Product_Category_ID) left join (select M_Product_Category_ID,AD_Language, Name from M_Product_Category_TRL) tableTRL3 on (table3.M_Product_Category_ID = tableTRL3.M_Product_Category_ID and tableTRL3.AD_Language = ?)  left join ad_ref_list_v list2 on (M_Product.Costtype = list2.value and list2.ad_reference_id = '800025' and list2.ad_language = ?)  left join (select C_TaxCategory_ID, Name from C_TaxCategory) table5 on (M_Product.C_TaxCategory_ID = table5.C_TaxCategory_ID) left join (select C_TaxCategory_ID,AD_Language, Name from C_TaxCategory_TRL) tableTRL5 on (table5.C_TaxCategory_ID = tableTRL5.C_TaxCategory_ID and tableTRL5.AD_Language = ?)  left join (select C_UOM_ID, Name from C_UOM) table7 on (M_Product.C_UOM_ID = table7.C_UOM_ID) left join (select C_UOM_ID,AD_Language, Name from C_UOM_TRL) tableTRL7 on (table7.C_UOM_ID = tableTRL7.C_UOM_ID and tableTRL7.AD_Language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND M_Product.M_Product_ID = ? " +
      "        AND M_Product.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND M_Product.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Producto616FA0E755174D728C5E3C5222AEF9ABData objectProducto616FA0E755174D728C5E3C5222AEF9ABData = new Producto616FA0E755174D728C5E3C5222AEF9ABData();
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.created = UtilSql.getValue(result, "created");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.updated = UtilSql.getValue(result, "updated");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.updatedby = UtilSql.getValue(result, "updatedby");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoFamiliaId = UtilSql.getValue(result, "em_atecco_familia_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoGenericoId = UtilSql.getValue(result, "em_atecco_generico_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.adImageId = UtilSql.getValue(result, "ad_image_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mBrandId = UtilSql.getValue(result, "m_brand_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mBrandIdr = UtilSql.getValue(result, "m_brand_idr");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.producttype = UtilSql.getValue(result, "producttype");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.producttyper = UtilSql.getValue(result, "producttyper");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.name2 = UtilSql.getValue(result, "name2");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isgeneric = UtilSql.getValue(result, "isgeneric");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoModelo = UtilSql.getValue(result, "em_atecco_modelo");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.value = UtilSql.getValue(result, "value");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoCodigoEdimca = UtilSql.getValue(result, "em_atecco_codigo_edimca");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoCodigoCotopaxi = UtilSql.getValue(result, "em_atecco_codigo_cotopaxi");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.genericProductId = UtilSql.getValue(result, "generic_product_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoTipo = UtilSql.getValue(result, "em_atecco_tipo");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoCaras = UtilSql.getValue(result, "em_atecco_caras");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoColor1 = UtilSql.getValue(result, "em_atecco_color1");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoColor2 = UtilSql.getValue(result, "em_atecco_color2");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoD1 = UtilSql.getValue(result, "em_atecco_d1");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.weight = UtilSql.getValue(result, "weight");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.upc = UtilSql.getValue(result, "upc");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.cUomWeightId = UtilSql.getValue(result, "c_uom_weight_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mProductCategoryId = UtilSql.getValue(result, "m_product_category_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mProductCategoryIdr = UtilSql.getValue(result, "m_product_category_idr");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.costtype = UtilSql.getValue(result, "costtype");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.costtyper = UtilSql.getValue(result, "costtyper");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mAttributesetId = UtilSql.getValue(result, "m_attributeset_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.cTaxcategoryId = UtilSql.getValue(result, "c_taxcategory_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.cTaxcategoryIdr = UtilSql.getValue(result, "c_taxcategory_idr");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoD2 = UtilSql.getValue(result, "em_atecco_d2");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoEspesor = UtilSql.getValue(result, "em_atecco_espesor");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoM3 = UtilSql.getValue(result, "em_atecco_m3");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.name = UtilSql.getValue(result, "name");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.description = UtilSql.getValue(result, "description");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.cUomId = UtilSql.getValue(result, "c_uom_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.cUomIdr = UtilSql.getValue(result, "c_uom_idr");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoTextura = UtilSql.getValue(result, "em_atecco_textura");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.delaymin = UtilSql.getValue(result, "delaymin");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isstocked = UtilSql.getValue(result, "isstocked");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.salesrepId = UtilSql.getValue(result, "salesrep_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.issold = UtilSql.getValue(result, "issold");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.ispurchased = UtilSql.getValue(result, "ispurchased");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isbom = UtilSql.getValue(result, "isbom");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isactive = UtilSql.getValue(result, "isactive");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.imageurl = UtilSql.getValue(result, "imageurl");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.descriptionurl = UtilSql.getValue(result, "descriptionurl");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.issummary = UtilSql.getValue(result, "issummary");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mLocatorId = UtilSql.getValue(result, "m_locator_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.volume = UtilSql.getValue(result, "volume");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.shelfwidth = UtilSql.getValue(result, "shelfwidth");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.shelfheight = UtilSql.getValue(result, "shelfheight");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.shelfdepth = UtilSql.getValue(result, "shelfdepth");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.unitsperpallet = UtilSql.getValue(result, "unitsperpallet");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.discontinued = UtilSql.getValue(result, "discontinued");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.discontinuedby = UtilSql.getDateValue(result, "discontinuedby", "dd-MM-yyyy");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isinvoiceprintdetails = UtilSql.getValue(result, "isinvoiceprintdetails");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.ispicklistprintdetails = UtilSql.getValue(result, "ispicklistprintdetails");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isverified = UtilSql.getValue(result, "isverified");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.sExpensetypeId = UtilSql.getValue(result, "s_expensetype_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isquantityvariable = UtilSql.getValue(result, "isquantityvariable");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.sResourceId = UtilSql.getValue(result, "s_resource_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.expplantype = UtilSql.getValue(result, "expplantype");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.periodnumberExp = UtilSql.getValue(result, "periodnumber_exp");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.defaultperiodExp = UtilSql.getValue(result, "defaultperiod_exp");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.calculated = UtilSql.getValue(result, "calculated");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.capacity = UtilSql.getValue(result, "capacity");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mrpPlannerId = UtilSql.getValue(result, "mrp_planner_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mrpPlanningmethodId = UtilSql.getValue(result, "mrp_planningmethod_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.qtymax = UtilSql.getValue(result, "qtymax");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.qtymin = UtilSql.getValue(result, "qtymin");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.qtystd = UtilSql.getValue(result, "qtystd");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.qtytype = UtilSql.getValue(result, "qtytype");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.stockmin = UtilSql.getValue(result, "stockmin");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.createvariants = UtilSql.getValue(result, "createvariants");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.updateinvariants = UtilSql.getValue(result, "updateinvariants");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.islinkedtoproduct = UtilSql.getValue(result, "islinkedtoproduct");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.prodCatSelection = UtilSql.getValue(result, "prod_cat_selection");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.productSelection = UtilSql.getValue(result, "product_selection");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.returnable = UtilSql.getValue(result, "returnable");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.overdueReturnDays = UtilSql.getValue(result, "overdue_return_days");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.ispricerulebased = UtilSql.getValue(result, "ispricerulebased");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.quantityRule = UtilSql.getValue(result, "quantity_rule");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.uniquePerDocument = UtilSql.getValue(result, "unique_per_document");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.printDescription = UtilSql.getValue(result, "print_description");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.relateprodcattoservice = UtilSql.getValue(result, "relateprodcattoservice");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.relateprodtoservice = UtilSql.getValue(result, "relateprodtoservice");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.allowDeferredSell = UtilSql.getValue(result, "allow_deferred_sell");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.deferredSellMaxDays = UtilSql.getValue(result, "deferred_sell_max_days");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.attrsetvaluetype = UtilSql.getValue(result, "attrsetvaluetype");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mProductId = UtilSql.getValue(result, "m_product_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.classification = UtilSql.getValue(result, "classification");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.sku = UtilSql.getValue(result, "sku");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.documentnote = UtilSql.getValue(result, "documentnote");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.help = UtilSql.getValue(result, "help");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.processing = UtilSql.getValue(result, "processing");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.stockMin = UtilSql.getValue(result, "stock_min");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.enforceAttribute = UtilSql.getValue(result, "enforce_attribute");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.maProcessplanId = UtilSql.getValue(result, "ma_processplan_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.guaranteedays = UtilSql.getValue(result, "guaranteedays");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mAttributesetinstanceId = UtilSql.getValue(result, "m_attributesetinstance_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.downloadurl = UtilSql.getValue(result, "downloadurl");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mFreightcategoryId = UtilSql.getValue(result, "m_freightcategory_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.ispriceprinted = UtilSql.getValue(result, "ispriceprinted");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.production = UtilSql.getValue(result, "production");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.coststd = UtilSql.getValue(result, "coststd");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.characteristicDesc = UtilSql.getValue(result, "characteristic_desc");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isdeferredrevenue = UtilSql.getValue(result, "isdeferredrevenue");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.bookusingpoprice = UtilSql.getValue(result, "bookusingpoprice");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.periodnumber = UtilSql.getValue(result, "periodnumber");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.defaultperiod = UtilSql.getValue(result, "defaultperiod");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.revplantype = UtilSql.getValue(result, "revplantype");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isdeferredexpense = UtilSql.getValue(result, "isdeferredexpense");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.managevariants = UtilSql.getValue(result, "managevariants");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.versionno = UtilSql.getValue(result, "versionno");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoCategoriaId = UtilSql.getValue(result, "em_atecco_categoria_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoSubCategoriaId = UtilSql.getValue(result, "em_atecco_sub_categoria_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.language = UtilSql.getValue(result, "language");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.adUserClient = "";
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.adOrgClient = "";
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.createdby = "";
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.trBgcolor = "";
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.totalCount = "";
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectProducto616FA0E755174D728C5E3C5222AEF9ABData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Producto616FA0E755174D728C5E3C5222AEF9ABData objectProducto616FA0E755174D728C5E3C5222AEF9ABData[] = new Producto616FA0E755174D728C5E3C5222AEF9ABData[vector.size()];
    vector.copyInto(objectProducto616FA0E755174D728C5E3C5222AEF9ABData);
    return(objectProducto616FA0E755174D728C5E3C5222AEF9ABData);
  }

/**
Select for relation
 */
  public static Producto616FA0E755174D728C5E3C5222AEF9ABData[] select(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String parValue, String parName, String parM_Product_Category_ID, String parProductType, String parC_TaxCategory_ID, String parSessionDate, String parSessionUser, String adUserClient, String adOrgClient, String orderbyclause)    throws ServletException {
    return select(connectionProvider, dateTimeFormat, paramLanguage, parValue, parName, parM_Product_Category_ID, parProductType, parC_TaxCategory_ID, parSessionDate, parSessionUser, adUserClient, adOrgClient, orderbyclause, 0, 0);
  }

/**
Select for relation
 */
  public static Producto616FA0E755174D728C5E3C5222AEF9ABData[] select(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String parValue, String parName, String parM_Product_Category_ID, String parProductType, String parC_TaxCategory_ID, String parSessionDate, String parSessionUser, String adUserClient, String adOrgClient, String orderbyclause, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(M_Product.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = M_Product.CreatedBy) as CreatedByR, " +
      "        to_char(M_Product.Updated, ?) as updated, " +
      "        to_char(M_Product.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        M_Product.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = M_Product.UpdatedBy) as UpdatedByR, " +
      "        M_Product.AD_Org_ID, " +
      "(CASE WHEN M_Product.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "M_Product.EM_Atecco_Familia_ID, " +
      "M_Product.EM_Atecco_Generico_ID, " +
      "M_Product.AD_Image_ID, " +
      "M_Product.M_Brand_ID, " +
      "(CASE WHEN M_Product.M_Brand_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS M_Brand_IDR, " +
      "M_Product.ProductType, " +
      "(CASE WHEN M_Product.ProductType IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS ProductTypeR, " +
      "M_Product.Name2, " +
      "COALESCE(M_Product.IsGeneric, 'N') AS IsGeneric, " +
      "M_Product.EM_Atecco_Modelo, " +
      "M_Product.Value, " +
      "M_Product.EM_Atecco_Codigo_Edimca, " +
      "M_Product.EM_Atecco_Codigo_Cotopaxi, " +
      "M_Product.Generic_Product_ID, " +
      "M_Product.EM_Atecco_Tipo, " +
      "M_Product.EM_Atecco_Caras, " +
      "M_Product.EM_Atecco_Color1, " +
      "M_Product.EM_Atecco_Color2, " +
      "M_Product.EM_Atecco_D1, " +
      "M_Product.Weight, " +
      "M_Product.UPC, " +
      "M_Product.C_Uom_Weight_ID, " +
      "M_Product.M_Product_Category_ID, " +
      "(CASE WHEN M_Product.M_Product_Category_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL3.Name IS NULL THEN TO_CHAR(table3.Name) ELSE TO_CHAR(tableTRL3.Name) END)), ''))),'') ) END) AS M_Product_Category_IDR, " +
      "M_Product.Costtype, " +
      "(CASE WHEN M_Product.Costtype IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS CosttypeR, " +
      "M_Product.M_AttributeSet_ID, " +
      "M_Product.C_TaxCategory_ID, " +
      "(CASE WHEN M_Product.C_TaxCategory_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL5.Name IS NULL THEN TO_CHAR(table5.Name) ELSE TO_CHAR(tableTRL5.Name) END)), ''))),'') ) END) AS C_TaxCategory_IDR, " +
      "M_Product.EM_Atecco_D2, " +
      "M_Product.EM_Atecco_Espesor, " +
      "M_Product.EM_Atecco_M3, " +
      "M_Product.Name, " +
      "M_Product.Description, " +
      "M_Product.C_UOM_ID, " +
      "(CASE WHEN M_Product.C_UOM_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL7.Name IS NULL THEN TO_CHAR(table7.Name) ELSE TO_CHAR(tableTRL7.Name) END)), ''))),'') ) END) AS C_UOM_IDR, " +
      "M_Product.EM_Atecco_Textura, " +
      "M_Product.Delaymin, " +
      "COALESCE(M_Product.IsStocked, 'N') AS IsStocked, " +
      "M_Product.SalesRep_ID, " +
      "COALESCE(M_Product.IsSold, 'N') AS IsSold, " +
      "COALESCE(M_Product.IsPurchased, 'N') AS IsPurchased, " +
      "COALESCE(M_Product.IsBOM, 'N') AS IsBOM, " +
      "COALESCE(M_Product.IsActive, 'N') AS IsActive, " +
      "M_Product.C_BPartner_ID, " +
      "M_Product.ImageURL, " +
      "M_Product.DescriptionURL, " +
      "COALESCE(M_Product.IsSummary, 'N') AS IsSummary, " +
      "M_Product.M_Locator_ID, " +
      "M_Product.Volume, " +
      "M_Product.ShelfWidth, " +
      "M_Product.ShelfHeight, " +
      "M_Product.ShelfDepth, " +
      "M_Product.UnitsPerPallet, " +
      "COALESCE(M_Product.Discontinued, 'N') AS Discontinued, " +
      "M_Product.DiscontinuedBy, " +
      "COALESCE(M_Product.IsInvoicePrintDetails, 'N') AS IsInvoicePrintDetails, " +
      "COALESCE(M_Product.IsPickListPrintDetails, 'N') AS IsPickListPrintDetails, " +
      "COALESCE(M_Product.IsVerified, 'N') AS IsVerified, " +
      "M_Product.S_ExpenseType_ID, " +
      "COALESCE(M_Product.Isquantityvariable, 'N') AS Isquantityvariable, " +
      "M_Product.S_Resource_ID, " +
      "M_Product.Expplantype, " +
      "M_Product.Periodnumber_Exp, " +
      "M_Product.DefaultPeriod_Exp, " +
      "COALESCE(M_Product.Calculated, 'N') AS Calculated, " +
      "M_Product.Capacity, " +
      "M_Product.MRP_Planner_ID, " +
      "M_Product.MRP_Planningmethod_ID, " +
      "M_Product.Qtymax, " +
      "M_Product.Qtymin, " +
      "M_Product.Qtystd, " +
      "COALESCE(M_Product.Qtytype, 'N') AS Qtytype, " +
      "M_Product.Stockmin, " +
      "M_Product.CreateVariants, " +
      "M_Product.Updateinvariants, " +
      "COALESCE(M_Product.Islinkedtoproduct, 'N') AS Islinkedtoproduct, " +
      "M_Product.Prod_Cat_Selection, " +
      "M_Product.Product_Selection, " +
      "COALESCE(M_Product.Returnable, 'N') AS Returnable, " +
      "M_Product.Overdue_Return_Days, " +
      "COALESCE(M_Product.Ispricerulebased, 'N') AS Ispricerulebased, " +
      "M_Product.Quantity_Rule, " +
      "COALESCE(M_Product.Unique_Per_Document, 'N') AS Unique_Per_Document, " +
      "COALESCE(M_Product.Print_Description, 'N') AS Print_Description, " +
      "M_Product.Relateprodcattoservice, " +
      "M_Product.Relateprodtoservice, " +
      "COALESCE(M_Product.Allow_Deferred_Sell, 'N') AS Allow_Deferred_Sell, " +
      "M_Product.Deferred_Sell_Max_Days, " +
      "M_Product.Attrsetvaluetype, " +
      "M_Product.M_Product_ID, " +
      "M_Product.Classification, " +
      "M_Product.AD_Client_ID, " +
      "M_Product.SKU, " +
      "M_Product.DocumentNote, " +
      "M_Product.Help, " +
      "M_Product.Processing, " +
      "M_Product.Stock_Min, " +
      "COALESCE(M_Product.Enforce_Attribute, 'N') AS Enforce_Attribute, " +
      "M_Product.MA_Processplan_ID, " +
      "M_Product.GuaranteeDays, " +
      "M_Product.M_AttributeSetInstance_ID, " +
      "M_Product.DownloadURL, " +
      "M_Product.M_FreightCategory_ID, " +
      "COALESCE(M_Product.Ispriceprinted, 'N') AS Ispriceprinted, " +
      "COALESCE(M_Product.Production, 'N') AS Production, " +
      "M_Product.Coststd, " +
      "M_Product.Characteristic_Desc, " +
      "COALESCE(M_Product.Isdeferredrevenue, 'N') AS Isdeferredrevenue, " +
      "COALESCE(M_Product.Bookusingpoprice, 'N') AS Bookusingpoprice, " +
      "M_Product.Periodnumber, " +
      "M_Product.DefaultPeriod, " +
      "M_Product.Revplantype, " +
      "COALESCE(M_Product.Isdeferredexpense, 'N') AS Isdeferredexpense, " +
      "M_Product.ManageVariants, " +
      "M_Product.VersionNo, " +
      "M_Product.EM_Atecco_Categoria_ID, " +
      "M_Product.EM_Atecco_Sub_Categoria_ID, " +
      "        '' AS TR_BGCOLOR, '' as total_count," +
      "        ? AS LANGUAGE, '' AS AD_USER_CLIENT, '' AS AD_ORG_CLIENT" +
      "        FROM M_Product left join (select AD_Org_ID, Name from AD_Org) table1 on (M_Product.AD_Org_ID = table1.AD_Org_ID) left join (select M_Brand_ID, Name from M_Brand) table2 on (M_Product.M_Brand_ID = table2.M_Brand_ID) left join ad_ref_list_v list1 on (M_Product.ProductType = list1.value and list1.ad_reference_id = '270' and list1.ad_language = ?)  left join (select M_Product_Category_ID, Name from M_Product_Category) table3 on (M_Product.M_Product_Category_ID = table3.M_Product_Category_ID) left join (select M_Product_Category_ID,AD_Language, Name from M_Product_Category_TRL) tableTRL3 on (table3.M_Product_Category_ID = tableTRL3.M_Product_Category_ID and tableTRL3.AD_Language = ?)  left join ad_ref_list_v list2 on (M_Product.Costtype = list2.value and list2.ad_reference_id = '800025' and list2.ad_language = ?)  left join (select C_TaxCategory_ID, Name from C_TaxCategory) table5 on (M_Product.C_TaxCategory_ID = table5.C_TaxCategory_ID) left join (select C_TaxCategory_ID,AD_Language, Name from C_TaxCategory_TRL) tableTRL5 on (table5.C_TaxCategory_ID = tableTRL5.C_TaxCategory_ID and tableTRL5.AD_Language = ?)  left join (select C_UOM_ID, Name from C_UOM) table7 on (M_Product.C_UOM_ID = table7.C_UOM_ID) left join (select C_UOM_ID,AD_Language, Name from C_UOM_TRL) tableTRL7 on (table7.C_UOM_ID = tableTRL7.C_UOM_ID and tableTRL7.AD_Language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((parValue==null || parValue.equals(""))?"":"  AND (M_Product.Value) LIKE (?) ");
    strSql = strSql + ((parName==null || parName.equals(""))?"":"  AND C_IGNORE_ACCENT(M_Product.Name) LIKE C_IGNORE_ACCENT(?) ");
    strSql = strSql + ((parM_Product_Category_ID==null || parM_Product_Category_ID.equals(""))?"":"  AND (M_Product.M_Product_Category_ID) = (?) ");
    strSql = strSql + ((parProductType==null || parProductType.equals(""))?"":"  AND (M_Product.ProductType) = (?) ");
    strSql = strSql + ((parC_TaxCategory_ID==null || parC_TaxCategory_ID.equals(""))?"":"  AND (M_Product.C_TaxCategory_ID) = (?) ");
    strSql = strSql + ((parSessionDate.equals("parSessionDate"))?"  AND 1 = 1 ":"");
    strSql = strSql + ((parSessionUser.equals("parSessionUser"))?"  AND 1 = 1 ":"");
    strSql = strSql + 
      "        AND M_Product.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND M_Product.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") " +
      "        ORDER BY ";
    strSql = strSql + ((orderbyclause==null || orderbyclause.equals(""))?"":orderbyclause);
    strSql = strSql + 
      ", 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (parValue != null && !(parValue.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, parValue);
      }
      if (parName != null && !(parName.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, parName);
      }
      if (parM_Product_Category_ID != null && !(parM_Product_Category_ID.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, parM_Product_Category_ID);
      }
      if (parProductType != null && !(parProductType.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, parProductType);
      }
      if (parC_TaxCategory_ID != null && !(parC_TaxCategory_ID.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, parC_TaxCategory_ID);
      }
      if (parSessionDate != null && !(parSessionDate.equals(""))) {
        }
      if (parSessionUser != null && !(parSessionUser.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (orderbyclause != null && !(orderbyclause.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Producto616FA0E755174D728C5E3C5222AEF9ABData objectProducto616FA0E755174D728C5E3C5222AEF9ABData = new Producto616FA0E755174D728C5E3C5222AEF9ABData();
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.created = UtilSql.getValue(result, "created");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.updated = UtilSql.getValue(result, "updated");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.updatedby = UtilSql.getValue(result, "updatedby");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoFamiliaId = UtilSql.getValue(result, "em_atecco_familia_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoGenericoId = UtilSql.getValue(result, "em_atecco_generico_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.adImageId = UtilSql.getValue(result, "ad_image_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mBrandId = UtilSql.getValue(result, "m_brand_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mBrandIdr = UtilSql.getValue(result, "m_brand_idr");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.producttype = UtilSql.getValue(result, "producttype");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.producttyper = UtilSql.getValue(result, "producttyper");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.name2 = UtilSql.getValue(result, "name2");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isgeneric = UtilSql.getValue(result, "isgeneric");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoModelo = UtilSql.getValue(result, "em_atecco_modelo");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.value = UtilSql.getValue(result, "value");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoCodigoEdimca = UtilSql.getValue(result, "em_atecco_codigo_edimca");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoCodigoCotopaxi = UtilSql.getValue(result, "em_atecco_codigo_cotopaxi");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.genericProductId = UtilSql.getValue(result, "generic_product_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoTipo = UtilSql.getValue(result, "em_atecco_tipo");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoCaras = UtilSql.getValue(result, "em_atecco_caras");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoColor1 = UtilSql.getValue(result, "em_atecco_color1");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoColor2 = UtilSql.getValue(result, "em_atecco_color2");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoD1 = UtilSql.getValue(result, "em_atecco_d1");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.weight = UtilSql.getValue(result, "weight");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.upc = UtilSql.getValue(result, "upc");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.cUomWeightId = UtilSql.getValue(result, "c_uom_weight_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mProductCategoryId = UtilSql.getValue(result, "m_product_category_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mProductCategoryIdr = UtilSql.getValue(result, "m_product_category_idr");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.costtype = UtilSql.getValue(result, "costtype");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.costtyper = UtilSql.getValue(result, "costtyper");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mAttributesetId = UtilSql.getValue(result, "m_attributeset_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.cTaxcategoryId = UtilSql.getValue(result, "c_taxcategory_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.cTaxcategoryIdr = UtilSql.getValue(result, "c_taxcategory_idr");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoD2 = UtilSql.getValue(result, "em_atecco_d2");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoEspesor = UtilSql.getValue(result, "em_atecco_espesor");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoM3 = UtilSql.getValue(result, "em_atecco_m3");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.name = UtilSql.getValue(result, "name");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.description = UtilSql.getValue(result, "description");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.cUomId = UtilSql.getValue(result, "c_uom_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.cUomIdr = UtilSql.getValue(result, "c_uom_idr");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoTextura = UtilSql.getValue(result, "em_atecco_textura");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.delaymin = UtilSql.getValue(result, "delaymin");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isstocked = UtilSql.getValue(result, "isstocked");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.salesrepId = UtilSql.getValue(result, "salesrep_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.issold = UtilSql.getValue(result, "issold");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.ispurchased = UtilSql.getValue(result, "ispurchased");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isbom = UtilSql.getValue(result, "isbom");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isactive = UtilSql.getValue(result, "isactive");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.imageurl = UtilSql.getValue(result, "imageurl");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.descriptionurl = UtilSql.getValue(result, "descriptionurl");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.issummary = UtilSql.getValue(result, "issummary");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mLocatorId = UtilSql.getValue(result, "m_locator_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.volume = UtilSql.getValue(result, "volume");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.shelfwidth = UtilSql.getValue(result, "shelfwidth");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.shelfheight = UtilSql.getValue(result, "shelfheight");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.shelfdepth = UtilSql.getValue(result, "shelfdepth");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.unitsperpallet = UtilSql.getValue(result, "unitsperpallet");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.discontinued = UtilSql.getValue(result, "discontinued");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.discontinuedby = UtilSql.getDateValue(result, "discontinuedby", "dd-MM-yyyy");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isinvoiceprintdetails = UtilSql.getValue(result, "isinvoiceprintdetails");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.ispicklistprintdetails = UtilSql.getValue(result, "ispicklistprintdetails");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isverified = UtilSql.getValue(result, "isverified");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.sExpensetypeId = UtilSql.getValue(result, "s_expensetype_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isquantityvariable = UtilSql.getValue(result, "isquantityvariable");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.sResourceId = UtilSql.getValue(result, "s_resource_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.expplantype = UtilSql.getValue(result, "expplantype");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.periodnumberExp = UtilSql.getValue(result, "periodnumber_exp");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.defaultperiodExp = UtilSql.getValue(result, "defaultperiod_exp");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.calculated = UtilSql.getValue(result, "calculated");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.capacity = UtilSql.getValue(result, "capacity");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mrpPlannerId = UtilSql.getValue(result, "mrp_planner_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mrpPlanningmethodId = UtilSql.getValue(result, "mrp_planningmethod_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.qtymax = UtilSql.getValue(result, "qtymax");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.qtymin = UtilSql.getValue(result, "qtymin");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.qtystd = UtilSql.getValue(result, "qtystd");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.qtytype = UtilSql.getValue(result, "qtytype");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.stockmin = UtilSql.getValue(result, "stockmin");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.createvariants = UtilSql.getValue(result, "createvariants");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.updateinvariants = UtilSql.getValue(result, "updateinvariants");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.islinkedtoproduct = UtilSql.getValue(result, "islinkedtoproduct");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.prodCatSelection = UtilSql.getValue(result, "prod_cat_selection");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.productSelection = UtilSql.getValue(result, "product_selection");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.returnable = UtilSql.getValue(result, "returnable");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.overdueReturnDays = UtilSql.getValue(result, "overdue_return_days");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.ispricerulebased = UtilSql.getValue(result, "ispricerulebased");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.quantityRule = UtilSql.getValue(result, "quantity_rule");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.uniquePerDocument = UtilSql.getValue(result, "unique_per_document");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.printDescription = UtilSql.getValue(result, "print_description");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.relateprodcattoservice = UtilSql.getValue(result, "relateprodcattoservice");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.relateprodtoservice = UtilSql.getValue(result, "relateprodtoservice");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.allowDeferredSell = UtilSql.getValue(result, "allow_deferred_sell");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.deferredSellMaxDays = UtilSql.getValue(result, "deferred_sell_max_days");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.attrsetvaluetype = UtilSql.getValue(result, "attrsetvaluetype");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mProductId = UtilSql.getValue(result, "m_product_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.classification = UtilSql.getValue(result, "classification");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.sku = UtilSql.getValue(result, "sku");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.documentnote = UtilSql.getValue(result, "documentnote");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.help = UtilSql.getValue(result, "help");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.processing = UtilSql.getValue(result, "processing");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.stockMin = UtilSql.getValue(result, "stock_min");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.enforceAttribute = UtilSql.getValue(result, "enforce_attribute");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.maProcessplanId = UtilSql.getValue(result, "ma_processplan_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.guaranteedays = UtilSql.getValue(result, "guaranteedays");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mAttributesetinstanceId = UtilSql.getValue(result, "m_attributesetinstance_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.downloadurl = UtilSql.getValue(result, "downloadurl");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.mFreightcategoryId = UtilSql.getValue(result, "m_freightcategory_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.ispriceprinted = UtilSql.getValue(result, "ispriceprinted");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.production = UtilSql.getValue(result, "production");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.coststd = UtilSql.getValue(result, "coststd");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.characteristicDesc = UtilSql.getValue(result, "characteristic_desc");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isdeferredrevenue = UtilSql.getValue(result, "isdeferredrevenue");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.bookusingpoprice = UtilSql.getValue(result, "bookusingpoprice");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.periodnumber = UtilSql.getValue(result, "periodnumber");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.defaultperiod = UtilSql.getValue(result, "defaultperiod");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.revplantype = UtilSql.getValue(result, "revplantype");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.isdeferredexpense = UtilSql.getValue(result, "isdeferredexpense");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.managevariants = UtilSql.getValue(result, "managevariants");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.versionno = UtilSql.getValue(result, "versionno");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoCategoriaId = UtilSql.getValue(result, "em_atecco_categoria_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.emAteccoSubCategoriaId = UtilSql.getValue(result, "em_atecco_sub_categoria_id");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.trBgcolor = UtilSql.getValue(result, "tr_bgcolor");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.totalCount = UtilSql.getValue(result, "total_count");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.language = UtilSql.getValue(result, "language");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.adUserClient = UtilSql.getValue(result, "ad_user_client");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.adOrgClient = UtilSql.getValue(result, "ad_org_client");
        objectProducto616FA0E755174D728C5E3C5222AEF9ABData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectProducto616FA0E755174D728C5E3C5222AEF9ABData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Producto616FA0E755174D728C5E3C5222AEF9ABData objectProducto616FA0E755174D728C5E3C5222AEF9ABData[] = new Producto616FA0E755174D728C5E3C5222AEF9ABData[vector.size()];
    vector.copyInto(objectProducto616FA0E755174D728C5E3C5222AEF9ABData);
    return(objectProducto616FA0E755174D728C5E3C5222AEF9ABData);
  }

/**
Create a registry
 */
  public static Producto616FA0E755174D728C5E3C5222AEF9ABData[] set(String mProductId, String adClientId, String adOrgId, String isactive, String createdby, String createdbyr, String updatedby, String updatedbyr, String name, String description, String issummary, String cUomId, String isstocked, String ispurchased, String issold, String volume, String weight, String revplantype, String quantityRule, String emAteccoModelo, String value, String mProductCategoryId, String cTaxcategoryId, String upc, String sku, String shelfwidth, String shelfheight, String shelfdepth, String unitsperpallet, String discontinued, String discontinuedby, String defaultperiod, String allowDeferredSell, String relateprodtoservice, String documentnote, String help, String classification, String salesrepId, String bookusingpoprice, String emAteccoGenericoId, String isbom, String isinvoiceprintdetails, String ispicklistprintdetails, String isverified, String processing, String islinkedtoproduct, String emAteccoTextura, String capacity, String delaymin, String mrpPlannerId, String mrpPlanningmethodId, String qtymax, String qtymin, String qtystd, String qtytype, String stockmin, String productSelection, String uniquePerDocument, String overdueReturnDays, String emAteccoSubCategoriaId, String relateprodcattoservice, String sExpensetypeId, String sResourceId, String emAteccoColor1, String emAteccoCaras, String emAteccoD1, String emAteccoCodigoEdimca, String ispricerulebased, String producttype, String imageurl, String descriptionurl, String versionno, String guaranteedays, String deferredSellMaxDays, String attrsetvaluetype, String adImageId, String cBpartnerId, String ispriceprinted, String name2, String costtype, String coststd, String stockMin, String enforceAttribute, String calculated, String maProcessplanId, String production, String isdeferredrevenue, String emAteccoD2, String mAttributesetId, String mAttributesetinstanceId, String emAteccoCodigoCotopaxi, String downloadurl, String mFreightcategoryId, String mLocatorId, String emAteccoTipo, String isdeferredexpense, String emAteccoCategoriaId, String printDescription, String prodCatSelection, String defaultperiodExp, String expplantype, String periodnumberExp, String emAteccoM3, String updateinvariants, String returnable, String emAteccoFamiliaId, String emAteccoColor2, String mBrandId, String isgeneric, String genericProductId, String createvariants, String characteristicDesc, String managevariants, String cUomWeightId, String isquantityvariable, String emAteccoEspesor, String periodnumber)    throws ServletException {
    Producto616FA0E755174D728C5E3C5222AEF9ABData objectProducto616FA0E755174D728C5E3C5222AEF9ABData[] = new Producto616FA0E755174D728C5E3C5222AEF9ABData[1];
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0] = new Producto616FA0E755174D728C5E3C5222AEF9ABData();
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].created = "";
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].createdbyr = createdbyr;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].updated = "";
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].updatedTimeStamp = "";
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].updatedby = updatedby;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].updatedbyr = updatedbyr;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].adOrgId = adOrgId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].adOrgIdr = "";
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].emAteccoFamiliaId = emAteccoFamiliaId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].emAteccoGenericoId = emAteccoGenericoId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].adImageId = adImageId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].mBrandId = mBrandId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].mBrandIdr = "";
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].producttype = producttype;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].producttyper = "";
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].name2 = name2;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].isgeneric = isgeneric;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].emAteccoModelo = emAteccoModelo;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].value = value;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].emAteccoCodigoEdimca = emAteccoCodigoEdimca;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].emAteccoCodigoCotopaxi = emAteccoCodigoCotopaxi;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].genericProductId = genericProductId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].emAteccoTipo = emAteccoTipo;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].emAteccoCaras = emAteccoCaras;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].emAteccoColor1 = emAteccoColor1;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].emAteccoColor2 = emAteccoColor2;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].emAteccoD1 = emAteccoD1;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].weight = weight;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].upc = upc;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].cUomWeightId = cUomWeightId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].mProductCategoryId = mProductCategoryId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].mProductCategoryIdr = "";
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].costtype = costtype;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].costtyper = "";
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].mAttributesetId = mAttributesetId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].cTaxcategoryId = cTaxcategoryId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].cTaxcategoryIdr = "";
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].emAteccoD2 = emAteccoD2;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].emAteccoEspesor = emAteccoEspesor;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].emAteccoM3 = emAteccoM3;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].name = name;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].description = description;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].cUomId = cUomId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].cUomIdr = "";
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].emAteccoTextura = emAteccoTextura;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].delaymin = delaymin;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].isstocked = isstocked;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].salesrepId = salesrepId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].issold = issold;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].ispurchased = ispurchased;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].isbom = isbom;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].isactive = isactive;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].cBpartnerId = cBpartnerId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].imageurl = imageurl;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].descriptionurl = descriptionurl;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].issummary = issummary;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].mLocatorId = mLocatorId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].volume = volume;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].shelfwidth = shelfwidth;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].shelfheight = shelfheight;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].shelfdepth = shelfdepth;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].unitsperpallet = unitsperpallet;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].discontinued = discontinued;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].discontinuedby = discontinuedby;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].isinvoiceprintdetails = isinvoiceprintdetails;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].ispicklistprintdetails = ispicklistprintdetails;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].isverified = isverified;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].sExpensetypeId = sExpensetypeId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].isquantityvariable = isquantityvariable;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].sResourceId = sResourceId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].expplantype = expplantype;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].periodnumberExp = periodnumberExp;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].defaultperiodExp = defaultperiodExp;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].calculated = calculated;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].capacity = capacity;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].mrpPlannerId = mrpPlannerId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].mrpPlanningmethodId = mrpPlanningmethodId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].qtymax = qtymax;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].qtymin = qtymin;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].qtystd = qtystd;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].qtytype = qtytype;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].stockmin = stockmin;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].createvariants = createvariants;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].updateinvariants = updateinvariants;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].islinkedtoproduct = islinkedtoproduct;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].prodCatSelection = prodCatSelection;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].productSelection = productSelection;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].returnable = returnable;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].overdueReturnDays = overdueReturnDays;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].ispricerulebased = ispricerulebased;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].quantityRule = quantityRule;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].uniquePerDocument = uniquePerDocument;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].printDescription = printDescription;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].relateprodcattoservice = relateprodcattoservice;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].relateprodtoservice = relateprodtoservice;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].allowDeferredSell = allowDeferredSell;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].deferredSellMaxDays = deferredSellMaxDays;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].attrsetvaluetype = attrsetvaluetype;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].mProductId = mProductId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].classification = classification;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].adClientId = adClientId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].sku = sku;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].documentnote = documentnote;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].help = help;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].processing = processing;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].stockMin = stockMin;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].enforceAttribute = enforceAttribute;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].maProcessplanId = maProcessplanId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].guaranteedays = guaranteedays;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].mAttributesetinstanceId = mAttributesetinstanceId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].downloadurl = downloadurl;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].mFreightcategoryId = mFreightcategoryId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].ispriceprinted = ispriceprinted;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].production = production;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].coststd = coststd;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].characteristicDesc = characteristicDesc;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].isdeferredrevenue = isdeferredrevenue;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].bookusingpoprice = bookusingpoprice;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].periodnumber = periodnumber;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].defaultperiod = defaultperiod;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].revplantype = revplantype;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].isdeferredexpense = isdeferredexpense;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].managevariants = managevariants;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].versionno = versionno;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].emAteccoCategoriaId = emAteccoCategoriaId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].emAteccoSubCategoriaId = emAteccoSubCategoriaId;
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].trBgcolor = "";
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].totalCount = "";
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].language = "";
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].adUserClient = "";
    objectProducto616FA0E755174D728C5E3C5222AEF9ABData[0].adOrgClient = "";
    return objectProducto616FA0E755174D728C5E3C5222AEF9ABData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef1407_0(ConnectionProvider connectionProvider, String CreatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as CreatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef1409_1(ConnectionProvider connectionProvider, String UpdatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as UpdatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2012(ConnectionProvider connectionProvider, String AD_ORG_ID, String AD_CLIENT_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT MAX(M_PRODUCT_CATEGORY_ID) FROM M_PRODUCT_CATEGORY WHERE AD_ISORGINCLUDED(?, AD_ORG_ID, ?) <> -1 AND ISDEFAULT = 'Y' AND AD_CLIENT_ID = ? AND ISSUMMARY='N' ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_ORG_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "max");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE M_Product" +
      "        SET AD_Org_ID = (?) , EM_Atecco_Familia_ID = (?) , EM_Atecco_Generico_ID = (?) , AD_Image_ID = (?) , M_Brand_ID = (?) , ProductType = (?) , Name2 = (?) , IsGeneric = (?) , EM_Atecco_Modelo = (?) , Value = (?) , EM_Atecco_Codigo_Edimca = (?) , EM_Atecco_Codigo_Cotopaxi = (?) , Generic_Product_ID = (?) , EM_Atecco_Tipo = (?) , EM_Atecco_Caras = (?) , EM_Atecco_Color1 = (?) , EM_Atecco_Color2 = (?) , EM_Atecco_D1 = TO_NUMBER(?) , Weight = TO_NUMBER(?) , UPC = (?) , C_Uom_Weight_ID = (?) , M_Product_Category_ID = (?) , Costtype = (?) , M_AttributeSet_ID = (?) , C_TaxCategory_ID = (?) , EM_Atecco_D2 = TO_NUMBER(?) , EM_Atecco_Espesor = TO_NUMBER(?) , EM_Atecco_M3 = TO_NUMBER(?) , Name = (?) , Description = (?) , C_UOM_ID = (?) , EM_Atecco_Textura = (?) , Delaymin = TO_NUMBER(?) , IsStocked = (?) , SalesRep_ID = (?) , IsSold = (?) , IsPurchased = (?) , IsBOM = (?) , IsActive = (?) , C_BPartner_ID = (?) , ImageURL = (?) , DescriptionURL = (?) , IsSummary = (?) , M_Locator_ID = (?) , Volume = TO_NUMBER(?) , ShelfWidth = TO_NUMBER(?) , ShelfHeight = TO_NUMBER(?) , ShelfDepth = TO_NUMBER(?) , UnitsPerPallet = TO_NUMBER(?) , Discontinued = (?) , DiscontinuedBy = TO_DATE(?) , IsInvoicePrintDetails = (?) , IsPickListPrintDetails = (?) , IsVerified = (?) , S_ExpenseType_ID = (?) , Isquantityvariable = (?) , S_Resource_ID = (?) , Expplantype = (?) , Periodnumber_Exp = TO_NUMBER(?) , DefaultPeriod_Exp = (?) , Calculated = (?) , Capacity = TO_NUMBER(?) , MRP_Planner_ID = (?) , MRP_Planningmethod_ID = (?) , Qtymax = TO_NUMBER(?) , Qtymin = TO_NUMBER(?) , Qtystd = TO_NUMBER(?) , Qtytype = (?) , Stockmin = TO_NUMBER(?) , CreateVariants = (?) , Updateinvariants = (?) , Islinkedtoproduct = (?) , Prod_Cat_Selection = (?) , Product_Selection = (?) , Returnable = (?) , Overdue_Return_Days = TO_NUMBER(?) , Ispricerulebased = (?) , Quantity_Rule = (?) , Unique_Per_Document = (?) , Print_Description = (?) , Relateprodcattoservice = (?) , Relateprodtoservice = (?) , Allow_Deferred_Sell = (?) , Deferred_Sell_Max_Days = TO_NUMBER(?) , Attrsetvaluetype = (?) , M_Product_ID = (?) , Classification = (?) , AD_Client_ID = (?) , SKU = (?) , DocumentNote = (?) , Help = (?) , Processing = (?) , Stock_Min = TO_NUMBER(?) , Enforce_Attribute = (?) , MA_Processplan_ID = (?) , GuaranteeDays = TO_NUMBER(?) , M_AttributeSetInstance_ID = (?) , DownloadURL = (?) , M_FreightCategory_ID = (?) , Ispriceprinted = (?) , Production = (?) , Coststd = TO_NUMBER(?) , Characteristic_Desc = (?) , Isdeferredrevenue = (?) , Bookusingpoprice = (?) , Periodnumber = TO_NUMBER(?) , DefaultPeriod = (?) , Revplantype = (?) , Isdeferredexpense = (?) , ManageVariants = (?) , VersionNo = (?) , EM_Atecco_Categoria_ID = (?) , EM_Atecco_Sub_Categoria_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE M_Product.M_Product_ID = ? " +
      "        AND M_Product.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND M_Product.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoFamiliaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoGenericoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adImageId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mBrandId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, producttype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isgeneric);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoModelo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, value);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCodigoEdimca);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCodigoCotopaxi);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, genericProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCaras);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoColor1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoColor2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoD1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, weight);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, upc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomWeightId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductCategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costtype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cTaxcategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoD2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoEspesor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoM3);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTextura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, delaymin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isstocked);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issold);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispurchased);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isbom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, imageurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descriptionurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issummary);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mLocatorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, volume);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfwidth);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfheight);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfdepth);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, unitsperpallet);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, discontinued);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, discontinuedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isinvoiceprintdetails);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispicklistprintdetails);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isverified);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sExpensetypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isquantityvariable);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sResourceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, expplantype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, periodnumberExp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, defaultperiodExp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculated);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, capacity);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mrpPlannerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mrpPlanningmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtymax);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtymin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtystd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtytype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, stockmin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createvariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updateinvariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, islinkedtoproduct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, prodCatSelection);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, productSelection);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, returnable);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, overdueReturnDays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispricerulebased);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quantityRule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, uniquePerDocument);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, printDescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, relateprodcattoservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, relateprodtoservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, allowDeferredSell);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deferredSellMaxDays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, attrsetvaluetype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, classification);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sku);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentnote);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, help);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, stockMin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, enforceAttribute);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, maProcessplanId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, guaranteedays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetinstanceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, downloadurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mFreightcategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispriceprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, production);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coststd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, characteristicDesc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdeferredrevenue);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bookusingpoprice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, periodnumber);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, defaultperiod);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, revplantype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdeferredexpense);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, managevariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, versionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCategoriaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoSubCategoriaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO M_Product " +
      "        (AD_Org_ID, EM_Atecco_Familia_ID, EM_Atecco_Generico_ID, AD_Image_ID, M_Brand_ID, ProductType, Name2, IsGeneric, EM_Atecco_Modelo, Value, EM_Atecco_Codigo_Edimca, EM_Atecco_Codigo_Cotopaxi, Generic_Product_ID, EM_Atecco_Tipo, EM_Atecco_Caras, EM_Atecco_Color1, EM_Atecco_Color2, EM_Atecco_D1, Weight, UPC, C_Uom_Weight_ID, M_Product_Category_ID, Costtype, M_AttributeSet_ID, C_TaxCategory_ID, EM_Atecco_D2, EM_Atecco_Espesor, EM_Atecco_M3, Name, Description, C_UOM_ID, EM_Atecco_Textura, Delaymin, IsStocked, SalesRep_ID, IsSold, IsPurchased, IsBOM, IsActive, C_BPartner_ID, ImageURL, DescriptionURL, IsSummary, M_Locator_ID, Volume, ShelfWidth, ShelfHeight, ShelfDepth, UnitsPerPallet, Discontinued, DiscontinuedBy, IsInvoicePrintDetails, IsPickListPrintDetails, IsVerified, S_ExpenseType_ID, Isquantityvariable, S_Resource_ID, Expplantype, Periodnumber_Exp, DefaultPeriod_Exp, Calculated, Capacity, MRP_Planner_ID, MRP_Planningmethod_ID, Qtymax, Qtymin, Qtystd, Qtytype, Stockmin, CreateVariants, Updateinvariants, Islinkedtoproduct, Prod_Cat_Selection, Product_Selection, Returnable, Overdue_Return_Days, Ispricerulebased, Quantity_Rule, Unique_Per_Document, Print_Description, Relateprodcattoservice, Relateprodtoservice, Allow_Deferred_Sell, Deferred_Sell_Max_Days, Attrsetvaluetype, M_Product_ID, Classification, AD_Client_ID, SKU, DocumentNote, Help, Processing, Stock_Min, Enforce_Attribute, MA_Processplan_ID, GuaranteeDays, M_AttributeSetInstance_ID, DownloadURL, M_FreightCategory_ID, Ispriceprinted, Production, Coststd, Characteristic_Desc, Isdeferredrevenue, Bookusingpoprice, Periodnumber, DefaultPeriod, Revplantype, Isdeferredexpense, ManageVariants, VersionNo, EM_Atecco_Categoria_ID, EM_Atecco_Sub_Categoria_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoFamiliaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoGenericoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adImageId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mBrandId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, producttype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isgeneric);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoModelo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, value);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCodigoEdimca);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCodigoCotopaxi);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, genericProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCaras);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoColor1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoColor2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoD1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, weight);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, upc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomWeightId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductCategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costtype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cTaxcategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoD2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoEspesor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoM3);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTextura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, delaymin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isstocked);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issold);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispurchased);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isbom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, imageurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descriptionurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issummary);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mLocatorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, volume);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfwidth);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfheight);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfdepth);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, unitsperpallet);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, discontinued);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, discontinuedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isinvoiceprintdetails);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispicklistprintdetails);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isverified);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sExpensetypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isquantityvariable);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sResourceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, expplantype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, periodnumberExp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, defaultperiodExp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculated);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, capacity);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mrpPlannerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mrpPlanningmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtymax);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtymin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtystd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtytype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, stockmin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createvariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updateinvariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, islinkedtoproduct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, prodCatSelection);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, productSelection);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, returnable);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, overdueReturnDays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispricerulebased);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quantityRule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, uniquePerDocument);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, printDescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, relateprodcattoservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, relateprodtoservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, allowDeferredSell);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deferredSellMaxDays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, attrsetvaluetype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, classification);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sku);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentnote);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, help);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, stockMin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, enforceAttribute);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, maProcessplanId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, guaranteedays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetinstanceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, downloadurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mFreightcategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispriceprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, production);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coststd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, characteristicDesc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdeferredrevenue);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bookusingpoprice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, periodnumber);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, defaultperiod);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, revplantype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdeferredexpense);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, managevariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, versionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCategoriaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoSubCategoriaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM M_Product" +
      "        WHERE M_Product.M_Product_ID = ? " +
      "        AND M_Product.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND M_Product.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM M_Product" +
      "         WHERE M_Product.M_Product_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM M_Product" +
      "         WHERE M_Product.M_Product_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
