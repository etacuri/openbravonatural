//Sqlc generated V1.O00-1
package com.atrums.ecommerce.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class ECOMTipoAnioData implements FieldProvider {
static Logger log4j = Logger.getLogger(ECOMTipoAnioData.class);
  private String InitRecordNumber="0";
  public String tipo;
  public String anio;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("tipo"))
      return tipo;
    else if (fieldName.equalsIgnoreCase("anio"))
      return anio;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static ECOMTipoAnioData[] selectModelo(ConnectionProvider connectionProvider, String ecomMarcaVehiculoId, String ecomModeloMarcasId)    throws ServletException {
    return selectModelo(connectionProvider, ecomMarcaVehiculoId, ecomModeloMarcasId, 0, 0);
  }

  public static ECOMTipoAnioData[] selectModelo(ConnectionProvider connectionProvider, String ecomMarcaVehiculoId, String ecomModeloMarcasId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	select e.tipo as tipo, " +
      "           e.year as anio" +
      "      from ecom_modelo_marcas e" +
      "     where e.ecom_marca_vehiculo_id = ?" +
      "       and e.ecom_modelo_marcas_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ecomMarcaVehiculoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ecomModeloMarcasId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ECOMTipoAnioData objectECOMTipoAnioData = new ECOMTipoAnioData();
        objectECOMTipoAnioData.tipo = UtilSql.getValue(result, "tipo");
        objectECOMTipoAnioData.anio = UtilSql.getValue(result, "anio");
        objectECOMTipoAnioData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectECOMTipoAnioData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ECOMTipoAnioData objectECOMTipoAnioData[] = new ECOMTipoAnioData[vector.size()];
    vector.copyInto(objectECOMTipoAnioData);
    return(objectECOMTipoAnioData);
  }
}
