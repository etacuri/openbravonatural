/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.compras.montos.data;

import java.util.Date;

import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.model.ad.access.Role;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity atecco_aprueba_compras (stored in table atecco_aprueba_compras).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class ateccoApruebaCompras extends BaseOBObject implements ClientEnabled, OrganizationEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "atecco_aprueba_compras";
    public static final String ENTITY_NAME = "atecco_aprueba_compras";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVO = "activo";
    public static final String PROPERTY_FECHACREACIN = "fechaCreacin";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_ACTUALIZADO = "actualizado";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_USUARIOCONTACTO = "usuarioContacto";
    public static final String PROPERTY_ROL = "rol";
    public static final String PROPERTY_MONTODESDE = "montoDesde";
    public static final String PROPERTY_MONTOHASTA = "montoHasta";

    public ateccoApruebaCompras() {
        setDefaultValue(PROPERTY_ACTIVO, true);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActivo() {
        return (Boolean) get(PROPERTY_ACTIVO);
    }

    public void setActivo(Boolean activo) {
        set(PROPERTY_ACTIVO, activo);
    }

    public Date getFechaCreacin() {
        return (Date) get(PROPERTY_FECHACREACIN);
    }

    public void setFechaCreacin(Date fechaCreacin) {
        set(PROPERTY_FECHACREACIN, fechaCreacin);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getActualizado() {
        return (Date) get(PROPERTY_ACTUALIZADO);
    }

    public void setActualizado(Date actualizado) {
        set(PROPERTY_ACTUALIZADO, actualizado);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public User getUsuarioContacto() {
        return (User) get(PROPERTY_USUARIOCONTACTO);
    }

    public void setUsuarioContacto(User usuarioContacto) {
        set(PROPERTY_USUARIOCONTACTO, usuarioContacto);
    }

    public Role getRol() {
        return (Role) get(PROPERTY_ROL);
    }

    public void setRol(Role rol) {
        set(PROPERTY_ROL, rol);
    }

    public Long getMontoDesde() {
        return (Long) get(PROPERTY_MONTODESDE);
    }

    public void setMontoDesde(Long montoDesde) {
        set(PROPERTY_MONTODESDE, montoDesde);
    }

    public Long getMontoHasta() {
        return (Long) get(PROPERTY_MONTOHASTA);
    }

    public void setMontoHasta(Long montoHasta) {
        set(PROPERTY_MONTOHASTA, montoHasta);
    }

}
